package gui.tableElements;

public interface Pld {

	public String[] constructColumns();
	public String[][] constructRows();
	public Integer[] getSegmentSize();
	
}
